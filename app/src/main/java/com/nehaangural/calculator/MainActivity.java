package com.nehaangural.calculator;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    Button buttonC,buttonCE,button1,button2,button3,button4,button5,button6,button7,button8,button9,button0,buttonadd, buttonsub, buttonmultiply, buttondivide, buttonequal,buttondot;
    double var1, var2;
    TextView textView;
    boolean Addition, Substraction, Multiplication, Division,decimal;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        button1 = findViewById(R.id.one);
        button2 = findViewById(R.id.two);
        button3= findViewById(R.id.three);
        button4= findViewById(R.id.four);
        button5 = findViewById(R.id.five);
        button6 = findViewById(R.id.six);
        button7= findViewById(R.id.seven);
        button8= findViewById(R.id.eight);
        button9= findViewById(R.id.nine);
        button0 = findViewById(R.id.zero);
        buttonadd = findViewById(R.id.add);
        buttonsub = findViewById(R.id.sub);
        buttonmultiply = findViewById(R.id.multiply);
        buttondivide = findViewById(R.id.div);
        buttonequal = findViewById(R.id.equal);
        buttonC=findViewById(R.id.button1);
        buttonCE=findViewById(R.id.button2);
        buttondot=findViewById(R.id.dot);
        textView = findViewById(R.id.textView);


        button1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                textView.setText(textView.getText()+"1");
            }
        });
        button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                textView.setText(textView.getText()+"2");
            }
        });
        button3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                textView.setText(textView.getText()+"3");
            }
        });
        button4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                textView.setText(textView.getText()+"4");
            }
        });
        button5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                textView.setText(textView.getText()+"5");
            }
        });
        button6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                textView.setText(textView.getText()+"6");
            }
        });
        button7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                textView.setText(textView.getText()+"7");
            }
        });
        button8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                textView.setText(textView.getText()+"8");
            }
        });
        button9.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                textView.setText(textView.getText()+"9");
            }
        });
        button0.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                textView.setText(textView.getText()+"0");
            }
        });
        buttonadd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (textView.getText().length() != 0) {
                    var1 = Float.parseFloat(textView.getText() + "");
                    Addition = true;
                    decimal = false;
                    textView.setText(null);
                }
            }
        });
        buttonsub.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (textView.getText().length() != 0) {
                    var1 = Float.parseFloat(textView.getText() + "");
                    Substraction = true;
                    decimal = false;
                    textView.setText(null);
                }
            }
        });
        buttonmultiply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (textView.getText().length() != 0) {
                    var1 = Float.parseFloat(textView.getText() + "");
                    Multiplication = true;
                    decimal = false;
                    textView.setText(null);
                }
            }
        });
        buttondivide.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (textView.getText().length() != 0) {
                    var1 = Float.parseFloat(textView.getText() + "");
                    Division = true;
                    decimal = false;
                    textView.setText(null);
                }
            }
        });

        buttonequal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Addition || Substraction || Multiplication || Division) {
                    var2 = Float.parseFloat(textView.getText() + "");

                }
                if (Addition) {
                    textView.setText(var1 + var2 + "");
                    Addition = false;
                }
                if (Substraction) {
                    textView.setText(var1 - var2 + "");
                    Substraction = false;
                }
                if (Multiplication) {
                    textView.setText(var1 * var2 + "");
                    Multiplication = false;
                }
                if (Division) {
                    textView.setText(var1 / var2 + "");
                    Division = false;
                }
            }
        });
        buttonC.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                textView.setText("");
                var1 = 0.0;
                var2 = 0.0;
            }
        });
        buttonCE.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                textView.setText("");
                var1 = 0.0;
                var2 = 0.0;
            }
        });
        buttondot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (decimal) {
                    //do nothing or you can show the error}
                } else {
                    textView.setText(textView.getText() + ".");
                    decimal = true;
                }
            }
        });
    }
}


